import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        System.out.println("Enter an integer to compute its factorial: ");
        Scanner in = new Scanner(System.in);
        int num = 0;

        try {
            num = in.nextInt();
            if (num < 0) {
                throw new IllegalArgumentException("Factorial is not defined for negative numbers");
            }
        } catch (java.util.InputMismatchException e) {
            System.out.println("Invalid input. Please enter a valid integer.");
            return;
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return;
        } finally {
            System.out.println("You have entered: " + num);
        }


        int answer = 1;
        int counter = 1;

        // Calculate factorial using a while loop
        while (counter <= num) {
            answer *= counter;
            counter++;
        }

        System.out.println("Factorial of " + num + " using while loop: " + answer);

        // Reset variables for the for loop
        answer = 1;


        // Calculate factorial using a for loop
        for (int i = 1; i <= num; i++) {
            answer *= i;
        }

        System.out.println("Factorial of " + num + " using for loop: " + answer);

        in.close();

    }
}